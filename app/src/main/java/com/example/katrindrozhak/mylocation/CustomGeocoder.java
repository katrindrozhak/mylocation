package com.example.katrindrozhak.mylocation;

import android.location.Location;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class CustomGeocoder extends AsyncTask<Location, Void, String> {

    private CustomGeocoderListener customGeocoderListener;

    public CustomGeocoder(CustomGeocoderListener listener) {
        this.customGeocoderListener = listener;
    }

    public JSONObject getLocationInfo(Location location) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            String google_apis_link = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
            String api_key = "&key=AIzaSyCol-dtg-H7LRZh3Dq9YqD-A-jntxXsgNI";
            HttpPost httppost = new HttpPost(google_apis_link + location.getLatitude() + "," + location.getLongitude() + api_key);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();

            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    public String getAddress(JSONObject jsonObject) {
        String address;

        try {

            JSONArray result = ((JSONArray) jsonObject.get("results"));
            JSONObject resultJSONObject = result.getJSONObject(0);
            JSONArray address_components = resultJSONObject.getJSONArray("address_components");
            JSONObject administrativeAreaLevel1 = null;

            for (int i = 0; i < address_components.length(); ++i) {
                JSONObject component = address_components.getJSONObject(i);
                JSONArray types = component.getJSONArray("types");
                String typeString = types.getString(0);
                if (typeString.equals("administrative_area_level_1")) {
                    administrativeAreaLevel1 = component;
                }
            }

            if (administrativeAreaLevel1 != null) {
                address = administrativeAreaLevel1.getString("long_name");
            } else {
                return "Not Found";
            }

        } catch (JSONException e) {
            return null;

        }
        return address;
    }

    @Override
    protected String doInBackground(Location... locations) {
        if (locations.length < 1) return null;
        JSONObject jsonObject = getLocationInfo(locations[0]);
        return getAddress(jsonObject);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        customGeocoderListener.onReceiveAddress(s);
    }
}
