package com.example.katrindrozhak.mylocation;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;

/**
 * Created by katrindrozhak on 30.03.17.
 */
@Table(database = LocationDatabase.class)
public class TableLocation extends BaseModel {
    @Column
    @PrimaryKey
    int id = new Date().hashCode();

    @Column
    public String cityName;

    @Column
    double latitude;

    @Column
    double longitude;
}
