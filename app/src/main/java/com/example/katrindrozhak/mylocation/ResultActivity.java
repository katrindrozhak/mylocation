package com.example.katrindrozhak.mylocation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by katrindrozhak on 30.03.17.
 */

public class ResultActivity extends AppCompatActivity {

    public List<TableLocation> tableLocationList = new ArrayList<>();
    private LocationAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        ListView listView = (ListView) findViewById(R.id.resultList);
        refresh();
        adapter = new LocationAdapter(this, tableLocationList);
        listView.setAdapter(adapter);
    }

    void refresh() {
        try {
            tableLocationList = new Select().from(TableLocation.class).queryList();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

}
