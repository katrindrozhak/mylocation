package com.example.katrindrozhak.mylocation;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by katrindrozhak on 30.03.17.
 */
@Database(name = LocationDatabase.NAME, version = LocationDatabase.VERSION)
public class LocationDatabase {
    public static final String NAME = "LocationDatabase";

    public static final int VERSION = 1;
}
