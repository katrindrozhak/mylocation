package com.example.katrindrozhak.mylocation;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

public class LocationPermissionsManager {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private AppCompatActivity mActivity;
    private LocationPermissionsManagerListener listener;

    public LocationPermissionsManager(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    public void askForLocationPermission() {
        if (!isPermissionGranted()) {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
        } else {
            listener.onPermissionResult(true);
        }
    }

    public boolean isPermissionGranted() {
        return ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    public void setListener(LocationPermissionsManagerListener listener) {
        this.listener = listener;
    }
}
