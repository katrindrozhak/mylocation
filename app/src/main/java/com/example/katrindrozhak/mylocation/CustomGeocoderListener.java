package com.example.katrindrozhak.mylocation;

/**
 * Created by katrindrozhak on 03.04.17.
 */

 public interface CustomGeocoderListener {
    void onReceiveAddress(String address);
}
