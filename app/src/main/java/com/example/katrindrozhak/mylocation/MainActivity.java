package com.example.katrindrozhak.mylocation;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import static com.example.katrindrozhak.mylocation.LocationPermissionsManager.MY_PERMISSIONS_REQUEST_LOCATION;

public class MainActivity extends AppCompatActivity implements LocationPermissionsManagerListener, ActivityCompat.OnRequestPermissionsResultCallback, MyListener {

    private TextView tvCityName;
    private ProgressBar progressBar;
    private TrackGPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvCityName = (TextView) findViewById(R.id.city_name_tv);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        gps = new TrackGPS(this);
        gps.setListener(this);

        LocationPermissionsManager lpm = new LocationPermissionsManager(this);
        lpm.setListener(this);

        if (lpm.isPermissionGranted()) {
            gps.getLocation();
        } else {
            lpm.askForLocationPermission();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GPSStateManager.LOCATION_SETTINGS_REQUEST) {
            gps.getLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION:
                boolean granted = (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED);
                if (granted) gps.getLocation();
                break;
        }
    }

    protected void refreshLocation(final Location location) {

        try {
            new CustomGeocoder(new CustomGeocoderListener() {
                @Override
                public void onReceiveAddress(String address) {
                    tvCityName.setText(address);
                    progressBar.setVisibility(View.INVISIBLE);
                    saveLocation(address, location);
                }
            }).execute(location);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveLocation(String cityName, Location location) {
        TableLocation tableLocation = new TableLocation();
        tableLocation.cityName = cityName;

        tableLocation.latitude = location.getLatitude();
        tableLocation.longitude = location.getLongitude();

        tableLocation.save();
    }

    public void onShowResult(View view) {
        Intent intent = new Intent(this, ResultActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        gps.stopUsingGPS();
    }

    @Override
    public void onReceiveNewLocation(Location location) {
        refreshLocation(location);
    }

    @Override
    public void askForGPS() {
        GPSStateManager.showSettingsAlertFrom(this);
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            gps.getLocation();
        }
    }
}
