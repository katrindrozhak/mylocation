package com.example.katrindrozhak.mylocation;

import android.location.Location;

/**
 * Created by katrindrozhak on 03.04.17.
 */

public interface MyListener {

    void onReceiveNewLocation(Location location);
    void askForGPS();
}
