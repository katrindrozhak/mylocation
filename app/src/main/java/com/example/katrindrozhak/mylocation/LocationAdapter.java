package com.example.katrindrozhak.mylocation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by katrindrozhak on 30.03.17.
 */

public class LocationAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<TableLocation> tableLocationList;

    LocationAdapter(Context context, List<TableLocation> locations) {
        tableLocationList = locations;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return tableLocationList.size();
    }

    @Override
    public Object getItem(int position) {
        return tableLocationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.item_activity, parent, false);
        }

        TableLocation tableLocation = getTableLocation(position);
        ((TextView) view.findViewById(R.id.city_name)).setText(tableLocation.cityName);
        ((TextView) view.findViewById(R.id.latitude)).setText("lat. " + tableLocation.latitude);
        ((TextView) view.findViewById(R.id.longitude)).setText("long. " + tableLocation.longitude);
        return view;
    }

    private TableLocation getTableLocation(int position) {
        return ((TableLocation) getItem(position));
    }
}
